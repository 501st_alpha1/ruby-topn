#!/usr/bin/env ruby

filename = ARGV[0]
n = ARGV[1]

unless filename and n
  print 'Usage: topn.rb <file> <N>'
  exit 1
end

unless n == n.to_i.to_s
  print 'Error: N must be an integer.'
  exit 2
end
n = n.to_i

unless File.exists?(filename)
  print 'Error: file ' + filename + ' does not exist.'
  exit 3
end

class PriorityQueue < Array
  def insert(obj)
    index = super.bsearch_index { |x| x >= obj }

    unless index
      index = length
    end

    super(index, obj)
  end
end

topN = PriorityQueue.new
File.open(filename).each do |line|
  number = line.to_i
  topN.insert(number)

  if (topN.length > n)
    topN = topN.drop(1)
  end
end

puts 'The top ' + n.to_s + ' numbers are:'
topN.reverse_each do |number|
  puts number
end
