# topN

A script to get the top N numbers from a large file containing one number per line.  Written as part of the GitLab Production Engineer interview process.

# Usage

```
./topn.rb somefile.txt 5
```

To use without a data file, in Bash do e.g.:

```
./topn.rb <(for i in {1..200}; do echo $RANDOM; done) 20
```
